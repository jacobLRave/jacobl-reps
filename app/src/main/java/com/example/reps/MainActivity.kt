package com.example.reps

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}

//     MainActivity->CategoriesFragment->CategoriesViewModel->CategoriesRepo->CategoriesViewModel
//    ->CategoriesFragment->CategoriesAdapter->CategoriesFragment
//     ->ListFragment->ListViewModel->ListRepo->ListViewModel->listFragment->ListAdapter->ListFragment