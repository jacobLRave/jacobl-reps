package com.example.reps.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.kety.adapter.HomeAdapter
import com.example.kety.adapter.ListAdapter
import com.example.kety.viewmodel.HomeViewModel

import com.example.reps.databinding.FragmentTypeBinding

class IntegerFragment : Fragment() {
    private var _binding: FragmentTypeBinding? = null
    private val binding get() = _binding!!
    private val homeViewModel by viewModels<HomeViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentTypeBinding.inflate(inflater, container, false).also {
        _binding = it

    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnBack.setOnClickListener() {
            findNavController().navigateUp()
        }
        homeViewModel.items.observe(viewLifecycleOwner) { category ->
            binding.rvListTop.apply {
                adapter = ListAdapter().apply {
                    addCategory(category[3].map { category -> "$category" })
                }
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}