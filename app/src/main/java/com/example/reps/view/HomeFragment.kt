package com.example.kety.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.kety.adapter.HomeAdapter
import com.example.kety.viewmodel.HomeViewModel
import com.example.reps.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val homeViewModel by viewModels<HomeViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also {
        _binding = it

    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeViewModel.items.observe(viewLifecycleOwner) { category ->
            binding.rvListTop.apply {
                adapter = HomeAdapter(::navigate).apply {
                    addCategory(category[0].map { category -> "$category" })
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun navigate(word: String) {
        when (word) {
            "String" -> findNavController().navigate(
                HomeFragmentDirections.actionHomeFragmentToStringFragment(

                )
            )
            "Boolean" -> findNavController().navigate(
                HomeFragmentDirections.actionHomeFragmentToBoleanFragment(

                )
            )
            "Integer" -> findNavController().navigate(
                HomeFragmentDirections.actionHomeFragmentToIntegerFragment(

                )
            )
            else -> {
                println("navigation did not work")
            }

        }


    }
}