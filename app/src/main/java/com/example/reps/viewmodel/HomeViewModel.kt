package com.example.kety.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.kety.model.RepsRepo

class HomeViewModel : ViewModel() {

    private val repo = RepsRepo

    private val _items = MutableLiveData<List<List<Any>>>()
    val items: LiveData<List<List<Any>>> get() = _items


    init {
        val items = repo.getItems()
        _items.value = items
    }


}