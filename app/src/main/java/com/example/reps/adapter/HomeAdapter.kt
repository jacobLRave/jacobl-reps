package com.example.kety.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.reps.databinding.ItemHomeBinding

//our input adapter takes in our navigation function and extendends our recyclerView adapter
//passing and it type constains it to only take in a view holder
class HomeAdapter(val nav: (String) -> Unit) :
    RecyclerView.Adapter<HomeAdapter.InputViewHolder>() {

    private var category = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InputViewHolder {
        // inflating a single item and determining where it is in the parent
        val binding = ItemHomeBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return InputViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InputViewHolder, position: Int) {
        // indexing current item we want to render to the view
        val word = category[position]
        holder.loadColor(word)
        // setting our on click listener
        holder.navigateClick().setOnClickListener() {
            nav(word)
        }
    }
    // tells onCreateViewHolder how many layouts to inflate.
    //  does this tell the onBindViewHolder how many layouts are being inflated?
    override fun getItemCount(): Int {
        return category.size
    }
    // adds the current word
    fun addCategory(category: List<String>) {
        //
        this.category = category.toMutableList()
        notifyDataSetChanged()
    }

    class InputViewHolder(
        private val binding: ItemHomeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadColor(word: String) {
            binding.mvcContainer.text = word
        }

        fun navigateClick(): TextView {
            return binding.mvcContainer
        }
    }

}