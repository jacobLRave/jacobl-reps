package com.example.kety.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.reps.databinding.ItemHomeBinding

class ListAdapter : RecyclerView.Adapter<ListAdapter.InputViewHolder>() {

    private var category = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InputViewHolder {
        val binding = ItemHomeBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return InputViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InputViewHolder, position: Int) {
        val word = category[position]
        holder.loadColor(word)
    }

    override fun getItemCount(): Int {
        return category.size
    }

    fun addCategory(category: List<String>) {
        this.category = category.toMutableList()
        notifyDataSetChanged()
    }

    class InputViewHolder(
        private val binding: ItemHomeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadColor(word: String) {
            binding.mvcContainer.text = word
        }

    }

}