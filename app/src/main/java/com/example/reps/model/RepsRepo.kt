package com.example.kety.model

object RepsRepo {

    private val itemsList = listOf(
        listOf(
            "String",
            "Boolean",
            "Integer",

            ),
        listOf(
            "This",
            "Is",
            "Not",
            "Very",
            "Dry",
            "But",
            "Thanks",
            "For",
            "The",
            "Reos"
        ),
        listOf(
            true,
            false,
            false,
            true,
            true,
            false,
            false,
            true,
            true,
            false
        ),
        listOf(
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10
        )
    )

    fun getItems(): List<List<Any>> {
        return itemsList
    }
}